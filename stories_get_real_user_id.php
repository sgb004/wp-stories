<?php
require_once __DIR__.'/stories_check_user.php';

function stories_get_real_user_id($user_id, $dieWithJSON = false){
	$ui = $user_id;

	if( current_user_can('administrator') ) {
		$result = stories_check_user($user_id);
	
		if(!$result['success']){
			if($dieWithJSON){
				wp_send_json($result);
				wp_die();
			}else{
				die($result['message']);
			}
		}
	}else{
		$ui = get_current_user_id();
	}

	return $ui;
}
?>