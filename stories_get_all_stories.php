<?php
function stories_get_user_display_name($user_id){
	$user_data = get_userdata($user_id);
	$display_name = '';

	if($user_data){
		$display_name = $user_data->display_name;
	}

	return $display_name;
}

function stories_get_user_data($user_id){
	return array(
		'avatar' => get_avatar_url($user_id, array('size' => 40)),
		'display_name' => stories_get_user_display_name($user_id),
		'url' => get_author_posts_url($user_id)
	);
}

function stories_get_all_stories($from_id){
	global $wpdb;
	$stories_max_download = get_option('stories_max_download', 12);
	if(!$stories_max_download){
		$stories_max_download = 12;
	}

	$fi = (int) $from_id;

	$table_name = $wpdb->prefix.'stories';
	$columns = 'id, user_id, type, title, link, registered';
	if($from_id > 0){
		$query = $wpdb->prepare('SELECT '.$columns.' FROM '.$table_name.' WHERE status = 1 AND id < %d ORDER BY registered DESC LIMIT '.$stories_max_download , $fi);
	}else{
		$query = "SELECT ".$columns." FROM ".$table_name." WHERE status = 1 ORDER BY registered DESC LIMIT ".$stories_max_download;
	}
	$items = array();
	$stories = $wpdb->get_results($query);
	$url = get_site_url().'?story=';
	$last_id = 0;

	if(sizeof($stories) > 0){
		$users_data = array();

		foreach($stories as $story){
			if(!isset($users_data[$story->user_id])){
				$users_data[$story->user_id] = stories_get_user_data($story->user_id);
			}
			$user_data = $users_data[$story->user_id];

			$items[] = array(
				'id' => $story->id,
				'src' => $url . $story->id,
				'type' => $story->type,
				'userAvatar' => $user_data['avatar'],
				'userName' => $user_data['display_name'],
				'title' => $story->title,
				'link' => $story->link,
				'registered' => $story->registered,
				'userUrl' => $user_data['url']
			);
		}

		$story = $wpdb->get_row('SELECT id FROM '.$table_name.' WHERE status = 1 ORDER BY registered ASC LIMIT 1');
		$last_id = $story->id;
	}

	return array("success" => true, "message" => "", "stories" => $items, "lastId" => $last_id);
}
?>