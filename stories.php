<?php
/**
* Plugin Name: Stories
* Description: This plugin allows you to add stories to users similar to Instagram stories.
* Version: 0.1
* Author: sgb004
**/

function stories_activation(){
	require_once __DIR__.'/activation.php';
	stories_activate();
}

/* FILTERS */

function stories_user_row_action($actions, $user) {
    $actions['stories'] = sprintf('<a href="%s">%s</a>', add_query_arg(array('action' => 'stories', 'user_id' => $user->ID), admin_url('admin.php')), __('Stories'));
    return $actions;
}

function stories_query_vars($vars){
	$vars[] = 'story';
    return $vars;
}

function stories_script_loader_tag($tag, $handle, $src){
	if ('stories-editor' !== $handle && 'stories-carousel' !== $handle) {
		return $tag;
	}
	$tag = '<script id="'.$handle.'-js" type="module" src="' . esc_url( $src ) . '"></script>';
	return $tag;
}

/* ACTIONS */

function stories_action_handler() {
    if (isset($_GET['action']) && $_GET['action'] === 'stories' && isset($_GET['user_id'])) {
        require_once 'stories-page.php';
        exit;
    }
}

function stories_ajax_upload_story(){
	require_once __DIR__.'/stories_upload_story.php';
	stories_upload_story();
}

function stories_ajax_update_story(){
	require_once __DIR__.'/stories_update_story.php';
	stories_update_story();
}

function stories_ajax_delete_story(){
	require_once __DIR__.'/stories_delete_story.php';
	stories_delete_story();
}

function stories_ajax_get_stories(){
	require_once __DIR__.'/stories_get_stories.php';
	stories_get_stories();
}

function stories_ajax_get_all_stories(){
	require_once __DIR__.'/stories_get_all_stories.php';
	$result = stories_get_all_stories((int) $_POST['fromId']);
	wp_send_json($result);
	die();
}

function stories_init() {
    add_rewrite_rule('^story/([0-9]+)/?', 'index.php?story=$matches[1]', 'top');
}

function stories_template_include( $template ) {
	if ( get_query_var( 'story' ) == false || get_query_var( 'story' ) == '' ) {
		return $template;
	}
 
	require_once __DIR__.'/stories_file.php';
	die();
}

function stories_enqueue_scripts(){
	global $post;
	if (has_shortcode( $post->post_content, "stories_user_editor") ) {
		require_once __DIR__.'/stories_load_editor_assets.php';
		stories_load_editor_assets();
	}else if (has_shortcode( $post->post_content, "stories_show_all") ) {
		require_once __DIR__.'/stories_load_carousel_assets.php';
		stories_load_carousel_assets();
	}
}

/* SHORTCODES */

function stories_shortcode_user_editor(){
	require_once __DIR__.'/stories_shortcode_user_editor_stories.php';
	return stories_shortcode_user_editor_stories();
}

function stories_shortcode_show_all(){
	require_once __DIR__.'/stories_shortcode_show_all_stories.php';
	return stories_shortcode_show_all_stories();
}

function stories_settings_admin_menu(){
	require_once __DIR__.'/stories-settings.php';
	stories_settings_menu();
}

function stories_settings_admin_init(){
	require_once __DIR__.'/stories-settings.php';
	stories_settings_init();
}

register_activation_hook( __FILE__, 'stories_activation');

add_filter('user_row_actions', 'stories_user_row_action', 10, 2);
add_filter('query_vars', 'stories_query_vars');
add_filter('script_loader_tag', 'stories_script_loader_tag' , 10, 3);

add_action('load-admin.php', 'stories_action_handler');
add_action('wp_ajax_upload_story', 'stories_ajax_upload_story'); 
add_action('wp_ajax_nopriv_upload_story', 'stories_ajax_upload_story');
add_action('wp_ajax_update_story', 'stories_ajax_update_story'); 
add_action('wp_ajax_nopriv_update_story', 'stories_ajax_update_story');
add_action('wp_ajax_delete_story', 'stories_ajax_delete_story');
add_action('wp_ajax_nopriv_delete_story', 'stories_ajax_delete_story');
add_action('wp_ajax_get_stories', 'stories_ajax_get_stories');
add_action('wp_ajax_nopriv_get_stories', 'stories_ajax_get_stories');
add_action('wp_ajax_get_all_stories', 'stories_ajax_get_all_stories');
add_action('wp_ajax_nopriv_get_all_stories', 'stories_ajax_get_all_stories');
add_action('init', 'stories_init');
add_action('template_include', 'stories_template_include');
add_action('wp_enqueue_scripts', 'stories_enqueue_scripts');
add_action('admin_menu', 'stories_settings_admin_menu');
add_action('admin_init', 'stories_settings_admin_init');

add_shortcode("stories_user_editor", 'stories_shortcode_user_editor');
add_shortcode("stories_show_all", 'stories_shortcode_show_all');
?>
