<?php

function stories_settings_menu() {
	add_menu_page(
		'Stories Settings',
		'Stories', 
		'manage_options',
		'stories-settings',
		'stories_settings_page'
	);
}

function stories_settings_init() {
	register_setting('stories-settings-group', 'stories_transition_time');
	register_setting('stories-settings-group', 'stories_force_video_time');
	//register_setting('stories-settings-group', 'stories_max_download');

	add_settings_section(
		'stories-settings-section',
		'',
		'',
		'stories-settings'
	);

	add_settings_field(
		'stories-transition-time',
		'Tiempo de transición entre historias en segundos',
		'stories_transition_time_callback',
		'stories-settings',
		'stories-settings-section'
	);
	
	/*
	add_settings_field(
		'stories-max-download',
		'Número máximo de descarga de historias',
		'stories_max_download_callback',
		'stories-settings',
		'stories-settings-section'
	);
	*/
}

function stories_settings_page() {
	?>
	<div class="wrap">
		<h2>Stories Settings</h2>
		<form method="post" action="options.php">
			<?php settings_fields('stories-settings-group'); ?>
			<?php do_settings_sections('stories-settings'); ?>
			<?php submit_button(); ?>
		</form>
	</div>
	<?php
}

function stories_transition_time_callback() {
	$value = get_option('stories_transition_time', 10);
	echo '<input type="number" name="stories_transition_time" value="' . esc_attr($value) . '" />';
}

function stories_max_download_callback() {
	$value = get_option('stories_max_download', 12);
	echo '<input type="number" name="stories_max_download" value="' . esc_attr($value) . '" />';
}
