<?php
require_once __DIR__.'/stories_check_user.php';

function stories_upload_story(){
	$user_id = (int) $_POST['userId'];
	$title = isset($_POST['title']) ? $_POST['title'] : '';
	$link = isset($_POST['link']) ? $_POST['link'] : '';

	$result = stories_check_user($user_id);
	

	if($result['success']){
		$result['success'] = false;
		$result['message'] = __("Not a valid file, select an image or mp4 video", 'stories');

		$src = $_POST['src'];
		$base64_string = substr($src, strpos($src, ",") + 1);
		$binary_string = base64_decode($base64_string);
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$tipo_mime = finfo_buffer($finfo, $binary_string);

		$type = explode('/', $tipo_mime);

		if($type[0] === 'image' || $tipo_mime === 'video/mp4'){
			global $wpdb;

			$result['message'] = __("There was an error to store the ".$type[0], 'stories');

			$resultInsert = $wpdb->insert( 
				$wpdb->prefix.'stories', 
				array( 
					'user_id' => $user_id, 
					'title' => trim($title), 
					'link' => trim($link), 
					'type' => $tipo_mime,
					'file' => $binary_string,
					'status' => 1,
					'registered' => date('Y-m-d H:i:s')
				) 
			);

			if ($resultInsert !== false) {
				$result['success'] = true;
				$result['message'] = __("Your ".$type[0]." was store", 'stories');
				$result['id'] = $wpdb->insert_id;
			}
		}
	}

    wp_send_json($result);
	wp_die();
}
?>