<?php
require_once ABSPATH . 'wp-admin/admin.php';
require_once ABSPATH . 'wp-admin/includes/translation-install.php';
require_once __DIR__ . '/stories_load_editor_assets.php';

add_action('admin_enqueue_scripts', 'stories_load_editor_assets');

$user_id = (int) $_GET['user_id'];
$user_data = get_userdata($user_id);

if (!$user_data) {
	wp_die( __( 'Invalid user ID.' ) );
}

if (is_multisite()
	&& ! current_user_can( 'manage_network_users' )
	&& $user_id !== $current_user->ID
	&& ! apply_filters( 'enable_edit_any_user_configuration', true )
) {
	wp_die( __( 'Sorry, you are not allowed to edit this user.' ) );
}

if (!current_user_can( 'edit_user', $user_id)) {
	wp_die( __( 'Sorry, you are not allowed to edit this user.' ) );
}

require_once ABSPATH . 'wp-admin/admin-header.php';
require_once __DIR__.'/stories_user_has_stories.php';

$user_has_stories = stories_user_has_stories($user_id);
?>

<div class="wrap stories-admin-page" id="profile-page">
	<ul class="subsubsub">
		<li><a href="users.php">< Return to Users</a></li>
	</ul>

	<?php require_once __DIR__ . '/stories-root-html.php' ?>
</div>

<?php
require_once ABSPATH . 'wp-admin/admin-footer.php';
?>