<?php
function stories_load_editor_assets() {
	wp_enqueue_style('stories', plugins_url('assets/ReactToastify.css', __FILE__), array(), '2.3');

	wp_enqueue_style('stories-editor', plugins_url('assets/stories-editor.css', __FILE__), array(), '2.3');
    wp_enqueue_script('stories-editor', plugins_url('assets/stories-editor.js', __FILE__), array(), '2.3', true);
}
?>