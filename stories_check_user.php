<?php
function stories_check_user($user_id){
	$result = array(
		'success' => false, 
		'message' => '',
	);
	$user_data = get_userdata($user_id);

	if (!$user_data) {
		$result['message'] = __( 'Invalid user ID.' );
	}else if (is_multisite()
		&& ! current_user_can( 'manage_network_users' )
		&& $user_id !== get_current_user_id()
		&& ! apply_filters( 'enable_edit_any_user_configuration', true )
	) {
		$result['message'] = __( 'Sorry, you are not allowed to edit this user.' );
	}else if (!current_user_can( 'edit_user', $user_id)) {
		$result['message'] = __( 'Sorry, you are not allowed to edit this user.' );
	}else{
		$result['success'] = true;
	}

	return $result;
}
?>