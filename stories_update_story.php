<?php
require_once __DIR__.'/stories_check_user.php';

function stories_update_story(){
	$user_id = (int) $_POST['userId'];

	$result = stories_check_user($user_id);
	

	if($result['success']){
		global $wpdb;

		$result['success'] = false;

		$table_name = $wpdb->prefix.'stories';
		$id = (int) $_POST['id'];
		$query = $wpdb->prepare('SELECT id FROM '.$table_name.' WHERE id = %d AND user_id = %d', $id, $user_id);
		$story = $wpdb->get_row($query);

		if(!$story->id){
			$result['message'] = __("The media does not exist", 'stories');
		}else{
			$status = $_POST['status'] == "true" ? 1 : 0;
			$title = $_POST['title'];
			$link = $_POST['link'];

			$resultUpdate = $wpdb->update($table_name, array('status' => $status, 'title' => $title, 'link' => $link), array('id' => $id, 'user_id' => $user_id));
			
			if ($resultUpdate !== false) {
				$result['success'] = true;
				$result['message'] = __("The status of the media was changed", 'stories');
			} else {
				$result['message'] = __("An error occurred while changing the status of the media", 'stories');
			}
		}
	}

    wp_send_json($result);
	wp_die();
}
?>