<?php
require_once __DIR__.'/stories_check_user.php';

function stories_delete_story(){
	$user_id = (int) $_POST['userId'];

	$result = stories_check_user($user_id);

	if($result['success']){
		global $wpdb;

		$id = (int) $_POST['id'];

		$resultDelete = $wpdb->delete( $wpdb->prefix.'stories', array('id' => $id, 'user_id' => $user_id));

		if ($resultDelete !== false) {
			$result['success'] = true;
			$result['message'] = __("The media was delete.", 'stories');
		} else {
			$result['success'] = false;
			$result['message'] = __("An error occurred while deleting the media", 'stories');
		}
	}

    wp_send_json($result);
	wp_die();
}
?>