<?php
require_once __DIR__.'/stories_load_editor_assets.php';
require_once __DIR__.'/stories_user_has_stories.php';

function stories_shortcode_user_editor_stories(){
	stories_load_editor_assets();

	$user_id = get_current_user_id();

	if($user_id == 0){
		return '<h2 class="stories-no-login">'.__("Log in to upload stories", "stories").'</h2>';
	}

	$user_data = get_userdata($user_id);

	$user_has_stories = stories_user_has_stories($user_id);

	ob_start();
	
	require __DIR__ . '/stories-root-html.php';

	return ob_get_clean();
}
?>