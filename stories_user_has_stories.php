<?php
require_once __DIR__.'/stories_get_real_user_id.php';

function stories_user_has_stories($user_id){
	global $wpdb;
	$ui = stories_get_real_user_id($user_id);

	$table_name = $wpdb->prefix.'stories';
	$query = $wpdb->prepare('SELECT id FROM '.$table_name.' WHERE user_id = %d LIMIT 1 ', $ui);
	$story = $wpdb->get_row($query);

	return !$story->id ? false : true;
}
?>