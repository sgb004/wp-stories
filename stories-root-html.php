<div 
	class="stories-editor-root"
	data-ajax-url="<?php echo admin_url('admin-ajax.php') ?>"
	data-file-url="<?php echo get_site_url().'?story=' ?>"
	data-user-id="<?php echo $user_id ?>"
	data-user-avatar="<?php echo get_avatar_url($user_id, array('size' => 150)); ?>"
	data-user-name="<?php echo $user_data->display_name; ?>"
	data-user-has-stories="<?php echo $user_has_stories ? 'true' : 'false' ?>">
</div>