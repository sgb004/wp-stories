<?php
function stories_load_carousel_assets() {
	wp_enqueue_style('stories', plugins_url('assets/ReactToastify.css', __FILE__), array(), '2.3');

	wp_enqueue_style('stories-carousel', plugins_url('assets/stories-carousel.css', __FILE__), array(), '2.3');
    wp_enqueue_script('stories-carousel', plugins_url('assets/stories-carousel.js', __FILE__), array(), '2.3', true);
}
?>