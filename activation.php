<?php
function stories_activate(){
	global $table_prefix, $wpdb;

	$stories_table = $table_prefix.'stories';

	if( $wpdb->get_var( "SHOW TABLES LIKE '$stories_table'" ) != $stories_table ) {

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE `$stories_table` (
			`id` int(11) NOT NULL auto_increment, 
			`title` varchar(255), 
			`link` varchar(255), 
			`user_id` bigint NOT NULL, 
			`type` varchar(20) NOT NULL, 
			`file` longblob NOT NULL, 
			`status` tinyint(1) DEFAULT true, 
			`registered` datetime NOT NULL, 
			PRIMARY KEY `id` (`id`) 
		) $charset_collate;";

		require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
	
		dbDelta( $sql );
	}
}
?>