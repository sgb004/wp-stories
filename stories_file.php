<?php
global $wpdb;

$slug = (int) $_GET['story'];

$table_name = $wpdb->prefix.'stories';
if( current_user_can('administrator') ) {
	$query = $wpdb->prepare('SELECT file FROM '.$table_name.' WHERE id = %d', $slug);
}else{
	$query = $wpdb->prepare('SELECT file FROM '.$table_name.' WHERE id = %d AND (status = 1 OR user_id = %d)', $slug, get_current_user_id());
}
$story = $wpdb->get_row($query);

if(!$story->file){
	status_header(404);
	get_header();
?>
<header class="page-header">
	<h1 class="page-title"><?php _e('File not found', 'stories'); ?></h1>
</header>
<?php
	get_footer();
}else{
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$type = finfo_buffer($finfo, $story->file);
	header("Content-type: ".$type);
	echo $story->file;
}
?>