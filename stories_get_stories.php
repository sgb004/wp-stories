<?php
require_once __DIR__.'/stories_get_real_user_id.php';

function stories_get_stories(){
	global $wpdb;
	$stories_max_download = get_option('stories_max_download', 12);
	if(!$stories_max_download){
		$stories_max_download = 12;
	}

	$user_id = (int) $_POST['userId'];
	$from_id = (int) $_POST['fromId'];

	$user_id = stories_get_real_user_id($user_id, true);
	

	$table_name = $wpdb->prefix.'stories';
	if($from_id > 0){
		$query = $wpdb->prepare('SELECT id, title, link, type, status FROM '.$table_name.' WHERE user_id = %d AND id < %d ORDER BY registered DESC LIMIT '.$stories_max_download, $user_id, $from_id);
	}else{
		$query = $wpdb->prepare('SELECT id, title, link, type, status FROM '.$table_name.' WHERE user_id = %d ORDER BY registered DESC LIMIT '.$stories_max_download, $user_id);
	}
	$stories = $wpdb->get_results($query);
	$last_id = 0;

	if(sizeof($stories) > 0){
		$query = $wpdb->prepare('SELECT id FROM '.$table_name.' WHERE user_id = %d ORDER BY registered ASC LIMIT 1', $user_id);
		$story = $wpdb->get_row($query);
		$last_id = $story->id;
	}

	$result = array("success" => true, "message" => "", "stories" => $stories, "lastId" => $last_id);

	wp_send_json($result);
	wp_die();
}
?>