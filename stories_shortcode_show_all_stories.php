<?php
require_once __DIR__.'/stories_load_carousel_assets.php';
require_once __DIR__.'/stories_get_all_stories.php';

function stories_shortcode_show_all_stories(){
	$data = stories_get_all_stories(0);
	$transition_time = get_option('stories_transition_time', 10);

	if(sizeof($data['stories']) > 0){
		stories_load_carousel_assets();

		ob_start();
		?>
		<div
			class="stories-carousel-root"
			data-items='<?php echo json_encode($data['stories']) ?>'
			data-ajax-url="<?php echo admin_url('admin-ajax.php') ?>"
			style="--transition-time: <?php echo $transition_time ?>s;"
		></div>
		<?php
		return ob_get_clean();
	}else{
		return '<h2 class="">'.__("There are no stories to show", "stories").'</h2>';
	}
}
?>